# tezos-reanalyze

## Executive summary

[`reanalyze`](https://github.com/reason-association/reanalyze) is easy to install and to execute. It runs fast :100:. Both the exception analysis and the dead code analysis seem interesting on tezos' sources. At first glance, all results of the exception analysis are interesting; while results of the dead code analysis require some filtering (not all warnings are interesting).

Regarding adoption, the dead code analysis is easier to adopt because it supports inference of annotations. It's too bad it's not there for the exception analysis (could we kindly ask for it or do it/delegate it?).

If you have more than one minute, I recommend looking at the analyses' results:

* Dead code analysis: [https://gitlab.com/smelc/tezos-reanalyze/-/blob/master/dce_log.txt](dce_log.txt)
* Exception analysis: [https://gitlab.com/smelc/tezos-reanalyze/-/blob/master/exception_log.txt](exception_log.txt)
* Termination analysis: [https://gitlab.com/smelc/tezos-reanalyze/-/blob/master/termination_log.txt](termination_log.txt)

## Prerequisite

* Having `reanalyze` in your `PATH`.
* Building `reanalyze` was straigthforward. I just followed the instructions. It supports ocaml 4.09.1 as used in tezos at the time I'm writing this.

## Usage

Copy `reanalyze.sh` to tezos' root directory and execute it.

`./reanalyze.sh` is a wrapper script around `reanalyze`'s CLI. It filters out `test` directories
within `src` and it excludes all protocols except `alpha` from the analysis (to avoid redundant warnings for now).
Calling `./reanalayze.sh` executes all analyses. Pass `-dce-cmt` (dead code elimination) or `-exception-cmt` to execute a single analysis instead.

## Results

`reanalyze` is fast. It executes all analyses in roughly 10 seconds on my ([@smelc](https://gitlab.com/smelc/tezos-reanalyze)) machine. Executing solely the dead code analysis yields the same duration. Executing solely the exception analyses executes in 6 seconds.

Here are the **analyses' outputs** (generated with `reanalyze` version `e28069497b0abbc051b03b2ca3065a3b7ec7c4c3` and tezos `88728775922c627bce326a5e8e452bb5aae969f3`):

* See [https://gitlab.com/smelc/tezos-reanalyze/-/blob/master/dce_log.txt](dce_log.txt) for the result of the dead code analysis.
* See [https://gitlab.com/smelc/tezos-reanalyze/-/blob/master/exception_log.txt](exception_log.txt) for the result of exception analysis.
* See [https://gitlab.com/smelc/tezos-reanalyze/-/blob/master/termination_log.txt](termination_log.txt) for the result of the termination analysis. This one doesn't report any issue so I suspect something is wrong :thinking_face:

## Zooming on some results

### Dead code analysis

* Looking at warnings within `src/lib_crypto/ed25519.ml`, the three warnings are about arguments that could be mandatory instead of being optional. I think we would like to turn such warnings OFF but I've not seen documentation about this being possible.
* Warnings within `lib_mockup/RPC_client.ml` are all wrong :worried: Is that because this code is within an object?
* Warning at `proto_alpha/lib_protocol/storage.mli` that `Contract.fold` is unused is correct.
* Warning about `proto_alpha/lib_protocol/script_repr.mli` that `node_cost` is unused is correct.
* Overall, circa `20%` of warnings are related to optional arguments. I think it would be nice to be able to ignore them for a start. Output of `reanalyze` is pretty easy to parse so a fast hackish solution would be to postprocess the output.

### Exception analysis

* First warning indicates a limitation of the tool.
* Second warning  (`lib_storage/context.ml:59`) is correct (`ref` can fail).
* Third warning (`lib_storage/context.ml:456`) is correct (`Bytes.create`).
* Warning in `src/proto_alpha/lib_client/mockup.ml:328` is correct (`Bytes.create`).
* Warning in `proto_alpha/lib_client/client_proto_multisig.ml:762` (get in `Array`).

It would be really nice to have inference of annotations, because we would be able to see what exceptions bubble up to high-level functions. Do they get a very long list of exceptions: I suppose yes, in that case could we shrink them?

## Adoption: dead code analysis

`reanalyze` has an interesting feature to ease its adaption: it can annotate existing source files, to avoid the burden of initially annotating everything.

Executing `./reanalyze.sh -write -dce-cmt` (executes solely dead code analysis and inserts corresponding annotations) yields:

* `286` modified files (recall that `./reanalyze.sh` ignores all protocols except `alpha`)
* Afterwards `make` fails with this error:

  ```
  File "src/proto_alpha/lib_protocol/misc.mli", line 47, characters 57-58:
  Error: Syntax error: 'end' expected
  File "src/proto_alpha/lib_protocol/functor.ml", line 9, characters 14-17:
  9 | module Misc : sig
                    ^^^
    This 'sig' might be unmatched
  make: *** [Makefile:44: all] Error 1
  ```

  These errors seem to be related with how the protocol is installed. Annotated code can be removed automatically with a dedicated ppx which I haven't tried.

Unfortunately `-write` doesn't do anything for the exception analysis.
