#!/usr/bin/env bash
#
# Script to execute reanalyze on tezos source code, taking care
# of filtering not so important data. Arguments are forwarded to reanalyze.
# If none, -all-cmt is passed. Pass -dce-cmt to executes soley
# the dead code analysis or -exception-cmt to execute solely the exception
# analysis.

echo "Not calling make. I'm assuming *.cmt files are up-to-date!"
command -v reanalyze &> /dev/null || { echo "You need reanalyze installed. Get it at https://github.com/reason-association/reanalyze"; exit 1; }

# For the moment, ignore all protocols except alpha
IGNORED_SRC_DIRS=("proto_000_Ps9mPmXa" "proto_001_PtCJ7pwo" "proto_002_PsYLVpVv" "proto_003_PsddFKi3" "proto_004_Pt24m4xi" "proto_005_PsBABY5H" "proto_005_PsBabyM1" "proto_006_PsCARTHA" "proto_demo_counter" "proto_demo_noops" "proto_genesis" "proto_genesis_carthagenet")  # proto_alpha is NOT ignored
SUPPRESS_ARG=""

function suppress() {
  [[ -n "$1" ]] || { echo "suppress expects one argument"; exit 1; }
  if [[ -n "$SUPPRESS_ARG" ]]; then
    SUPPRESS_ARG+=","
  fi
  SUPPRESS_ARG+="$1"
}

for IGNORED_SRC_DIR in ${IGNORED_SRC_DIRS[*]}
do
  suppress "src/$IGNORED_SRC_DIR"
done

ALL_SRC_DIRS=$(find src -type d)
for SRC_DIR in $ALL_SRC_DIRS
do
  if [[ "$SRC_DIR" == *"/test"* ]]; then
    suppress "$SRC_DIR"
  fi
done

if [[ "$#" == "0" ]]; then
  echo "No argument specified for reanalyze. Using -all-cmt"
  CMD_ARG="-all-cmt"
else
  CMD_ARG=$*
fi

CMD="reanalyze $CMD_ARG _build/default/src -suppress $SUPPRESS_ARG"
echo "$CMD"
$CMD
